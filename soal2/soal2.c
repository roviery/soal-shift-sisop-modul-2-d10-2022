#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>

struct drakor{
    char name[100];
    char year[10];
}drk[100];

int main(){

    pid_t child;
    int status;

    child = fork();


    if (child < 0) exit(EXIT_FAILURE);
    else if (child == 0){
        char *argv[] = {"mkdir", "/home/roviery/shift2", NULL};
        execv("/bin/mkdir", argv);
    }
    else {
        while((wait(&status)) > 0);

        // 2a
        /*====== Membuat directory drakor =======*/
        child = fork();

        if (child < 0) exit(EXIT_FAILURE);
        else if (child == 0){
            char *argv[] = {"mkdir", "/home/roviery/shift2/drakor", NULL};
            execv("/bin/mkdir", argv);  
        }else while((wait(&status)) > 0);

        /*====== unzip drakor.zip =======*/

        child = fork();

        if (child < 0) exit(EXIT_FAILURE);
        else if (child == 0){
            char *argv[] = {"unzip", "drakor.zip", "-d", "/home/roviery/shift2/drakor", NULL};
            execv("/bin/unzip", argv);  
        }else while((wait(&status)) > 0);

        /*====== delete folder tidak penting =======*/
        
        DIR *dp;
        struct dirent *ep;
        char path[100] = "/home/roviery/shift2/drakor";

        dp = opendir(path);

        if (dp != NULL){
            while ((ep = readdir(dp))){
                char folderPath[100];

                memset(folderPath, 0, strlen(folderPath));
                strcat(folderPath, "/home/roviery/shift2/drakor/");
                strcat(folderPath, ep->d_name);

                if (ep->d_type == 4){
                    child = fork();

                    if (child < 0) exit(EXIT_FAILURE);
                    else if (child == 0){
                        char *argv[] = {"rm", "-rf", folderPath, NULL};
                        execv("/bin/rm", argv); 
                    }else while((wait(&status)) > 0);
                }
            }
        }

        // 2b & 2c & 2d
        /*====== buat folder dengan membaca nama file =======*/

        /* 
        kasus normal (all-of-us-are-dead;2022;thriller.png)
        dan double awal (love-alarm;2019;romance_who-are-you;2015;school.png) -> love-alarm;2019;romance
        */
        dp = opendir(path);
        if (dp != NULL){
            while ((ep = readdir(dp))){
                char *folderName;
                char fileName[100];
                char rename[100];
                char renamePath[100];
                int counter = 1;

                strcpy(fileName, ep->d_name);
                folderName = strtok(ep->d_name, ";");

                while(folderName != NULL){
                    if (counter == 1) {
                        strcpy(rename, folderName);
                        strcat(rename, ".png");
                    }
                    if (counter == 3){
                        char folderPath[100];
                        char filePath[100];

                        if (strstr(folderName, "_") != NULL){
                            folderName = strtok(folderName, "_"); // -> tanpa _...

                            memset(folderPath, 0, strlen(folderPath));
                            strcat(folderPath, "/home/roviery/shift2/drakor/");
                            strcat(folderPath, folderName);

                            child = fork();

                            if (child < 0) exit(EXIT_FAILURE);
                            else if (child == 0){
                                char *argv[] = {"mkdir", "-p", folderPath, NULL};
                                execv("/bin/mkdir", argv); 
                            }else while((wait(&status)) > 0);
                        }
                        else if (strstr(folderName, ".png") != NULL){
                            folderName = strtok(folderName, "."); // -> tanpa extension .png

                            memset(folderPath, 0, strlen(folderPath));
                            strcat(folderPath, "/home/roviery/shift2/drakor/");
                            strcat(folderPath, folderName);

                            child = fork();

                            if (child < 0) exit(EXIT_FAILURE);
                            else if (child == 0){
                                char *argv[] = {"mkdir", "-p", folderPath, NULL};
                                execv("/bin/mkdir", argv); 
                            }else while((wait(&status)) > 0);
                        }


                        // copy file ke directory genre

                        memset(filePath, 0, strlen(filePath));
                        strcat(filePath, "/home/roviery/shift2/drakor/");
                        strcat(filePath, fileName);

                        memset(renamePath, 0, strlen(renamePath));
                        strcat(folderPath, "/");
                        strcat(renamePath, folderPath);
                        strcat(folderPath, fileName);
                        strcat(renamePath, rename);

                        child = fork();

                        if (child < 0) exit(EXIT_FAILURE);
                        else if (child == 0){
                            char *argv[] = {"cp", filePath, folderPath, NULL};
                            execv("/bin/cp", argv); 
                        }else {
                            while((wait(&status)) > 0);

                            // rename 

                            child = fork();

                            if (child < 0) exit(EXIT_FAILURE);
                            else if (child == 0){
                                char *argv[] = {"mv", folderPath, renamePath, NULL};
                                execv("/bin/mv", argv);
                            }else while((wait(&status)) > 0);      
                        }

                        break;
                    }
                    folderName = strtok(NULL, ";");
                    counter++;
                }
            }
        }

        // kasus double akhir (love-alarm;2019;romance_who-are-you;2015;school.png) -> who-are-you;2015;school.png
        dp = opendir(path);
        if (dp != NULL){
            while ((ep = readdir(dp))){
                if (strstr(ep->d_name, "_") != NULL){
                    char *folderName;
                    char fileName[100];
                    char rename[100];
                    char renamePath[100];
                    int counter = 1;

                    strcpy(fileName, ep->d_name);
                    folderName = strtok(ep->d_name, "_");

                    while(folderName != NULL){
                        if (counter == 2) break;

                        folderName = strtok(NULL, "_");
                        counter++;
                    }

                    counter = 1;
                    folderName = strtok(folderName, ";");
                    while(folderName != NULL){
                        if (counter == 1){
                            strcpy(rename, folderName);
                            strcat(rename, ".png");
                        }
                        if (counter == 3){
                            char folderPath[100];
                            char filePath[100];
                            if (strstr(folderName, ".png") != NULL){
                                folderName = strtok(folderName, ".");

                                memset(folderPath, 0, strlen(folderPath));
                                strcat(folderPath, "/home/roviery/shift2/drakor/");
                                strcat(folderPath, folderName);

                                child = fork();

                                if (child < 0) exit(EXIT_FAILURE);
                                else if (child == 0){
                                    char *argv[] = {"mkdir", "-p", folderPath, NULL};
                                    execv("/bin/mkdir", argv); 
                                }else while((wait(&status)) > 0);
                            }

                            // copy file ke directory genre

                            memset(filePath, 0, strlen(filePath));
                            strcat(filePath, "/home/roviery/shift2/drakor/");
                            strcat(filePath, fileName);

                            memset(renamePath, 0, strlen(renamePath));
                            strcat(folderPath, "/");
                            strcat(renamePath, folderPath);
                            strcat(folderPath, fileName);
                            strcat(renamePath, rename);

                            child = fork();

                            if (child < 0) exit(EXIT_FAILURE);
                            else if (child == 0){
                                char *argv[] = {"cp", filePath, folderPath, NULL};
                                execv("/bin/cp", argv); 
                            }else {
                                while((wait(&status)) > 0);

                                child = fork();

                                if (child < 0) exit(EXIT_FAILURE);
                                else if (child == 0){
                                    char *argv[] = {"mv", folderPath, renamePath, NULL};
                                    execv("/bin/mv", argv); 
                                }else while((wait(&status)) > 0);    
                            }
                            break;
                        }
                        folderName = strtok(NULL, ";");
                        counter++;
                    }
                }
            }
        }
    
        // 2e
        // all-of-us-are-dead;2022;thriller.png & double awal
        dp = opendir(path);
        if (dp != NULL){
            while (ep = readdir(dp)){
                if (ep->d_type == 4) continue;
                char *tok;
                char fileName[100];
                char tahun[100];
                char genre[100];
                int counter = 1;

                tok = strtok(ep->d_name, ";");

                while(tok != NULL){
                    if (counter == 1)
                        strcpy(fileName, tok);
                    if (counter == 2)
                        strcpy(tahun, tok);
                    if (counter == 3){
                        if (strstr(tok, "_") != NULL){
                            int c = 1;
                            tok = strtok(tok, "_");
                            strcpy(genre, tok);
                        }
                        else if (strstr(tok, ".png") != NULL){
                            tok = strtok(tok, ".");
                            strcpy(genre, tok);
                        } 
                    }

                    tok = strtok(NULL, ";");
                    counter++;
        
                }

                FILE *outputFile;
                char directoryGenre[100];
                strcpy(directoryGenre, path);
                strcat(directoryGenre, "/");
                strcat(directoryGenre, genre);
                strcat(directoryGenre, "/data.txt");

                outputFile = fopen(directoryGenre, "a");

                fprintf(outputFile, "%s\n", fileName);
                fprintf(outputFile, "%s\n", tahun);

                fclose(outputFile);

            }
        }
    
        // double akhir
        // reply1988;2015;comedy_nightmare-teacher;2016;horror.png
        dp = opendir(path);
        if (dp != NULL){
            while((ep = readdir(dp))){
                if (strstr(ep->d_name, "_") != NULL){
                    
                    if (ep->d_type == 4) continue;
                    char *tok;
                    char fileName[100];
                    char tahun[100];
                    char genre[100];
                    int counter = 1;

                    tok = strtok(ep->d_name, "_");

                    while(tok != NULL){
                        if (counter == 2) break;
                        tok = strtok(NULL, "_");
                        counter++;
                    }

                    counter = 1;
                    tok = strtok(tok, ";");

                    while(tok != NULL){
                        if (counter == 1)
                            strcpy(fileName, tok);
                        if (counter == 2)
                            strcpy(tahun, tok);
                        if (counter == 3){
                            if (strstr(tok, ".png") != NULL){
                                tok = strtok(tok, ".");
                                strcpy(genre, tok);
                            } 
                        }

                        tok = strtok(NULL, ";");
                        counter++;
                    }

                    FILE *outputFile;
                    char directoryGenre[100];
                    strcpy(directoryGenre, path);
                    strcat(directoryGenre, "/");
                    strcat(directoryGenre, genre);
                    strcat(directoryGenre, "/data.txt");
                    outputFile = fopen(directoryGenre, "a");

                    fprintf(outputFile, "%s\n", fileName);
                    fprintf(outputFile, "%s\n", tahun);

                    fclose(outputFile);

                }
            }
        }
    
        // sorting
        dp = opendir(path);
        if (dp != NULL){
            while((ep = readdir(dp))){
                if (ep->d_type == 4){
                    if (strstr(ep->d_name, "..") != NULL) continue;
                    if (strstr(ep->d_name, ".") != NULL) continue;
                    char dataPath[100];
                    char ch;
                    char name[100];
                    char year[100];
                    strcpy(dataPath, path);
                    strcat(dataPath, "/");
                    strcat(dataPath, ep->d_name);
                    strcat(dataPath, "/data.txt");

                    FILE *readFile;
                    readFile = fopen(dataPath, "r");

                    int index = 0;
                    int line = 1; 
                    while(!feof(readFile)){
                        if (line%2 == 1){
                            fscanf(readFile, "%s", drk[index].name);
                            // printf("\nName = %s\n", drk[index].name);
                        }
                        else{
                            fscanf(readFile, "%s", drk[index].year);
                            // printf("\nYear = %s\n", drk[index].year);
                            index++;
                        }
                        line++;
                    }

                    for (int i = 0; i<index-1; i++){
                        for (int j = 0; j<index-i-1 ; j++){
                            int y1 = atoi(drk[j].year);
                            int y2 = atoi(drk[j+1].year);
                            if (y1 > y2){
                                char temp[100];
                                strcpy(temp, drk[j].year);
                                strcpy(drk[j].year, drk[j+1].year);
                                strcpy(drk[j+1].year, temp);

                                strcpy(temp, drk[j].name);
                                strcpy(drk[j].name, drk[j+1].name);
                                strcpy(drk[j+1].name, temp);
                            }

                        }
                    }

                    

                    FILE *writeFile;
                    writeFile = fopen(dataPath, "w");
                    fclose(writeFile);
                    writeFile = fopen(dataPath, "a");

                    fprintf(writeFile, "Kategori: %s\n", ep->d_name);

                    for (int i = 0; i<index; i++){
                        fprintf(writeFile, "\nNama : %s\n", drk[i].name);
                        fprintf(writeFile, "Tahun : %s\n", drk[i].year);

                    }

                    fclose(readFile);
                    fclose(writeFile);
                }
            }
        }
        
        // Delete file
        dp = opendir(path);
        if (dp != NULL){
            while((ep = readdir(dp))){
                if (ep->d_type == 4) continue;

                char filePath[100];

                strcpy(filePath, "/home/roviery/shift2/drakor/");
                strcat(filePath, ep->d_name);

                remove(filePath);
            }
        }
    
    }

   
    


    return 0;
}