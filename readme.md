# Soal Shift Modul 1 - D10
Penyelesaian Soal Shift Modul 1 Sistem Operasi 2021\
Kelompok D10
* Halyusa Ard Wahyudi - 5025201088
* Zahra Fayyadiyati - 5025201133
* Nathanael Roviery - 5025201258

---
## Table of Contents
* [Soal 1](#soal-1)
    * [Soal 1.a](#soal-1a)
    * [Soal 1.b](#soal-1b)
    * [Soal 1.c](#soal-1c)
    * [Soal 1.d](#soal-1d)
    * [Soal 1.e](#soal-1e)
* [Soal 2](#soal-2)
    * [Soal 2.a](#soal-2a)
    * [Soal 2.b](#soal-2b)
    * [Soal 2.c](#soal-2c)
    * [Soal 2.d](#soal-2d)
    * [Soal 2.e](#soal-2e)
* [Soal 3](#soal-3)
    * [Soal 3.a](#soal-3a)
    * [Soal 3.b](#soal-3b)
    * [Soal 3.c](#soal-3c)
    * [Soal 3.d](#soal-3d)
    * [Soal 3.e](#soal-3e)

# Soal 1
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal1/soal1.c)

**Deskripsi:**
Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

## Soal 1.a
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal1/soal1.c)

**Deskripsi:**
Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut.

**Pembahasan:**
Proses download akan dilakukan oleh command bash `wget`, sementara pengekstrakan file zip akan dilakukan oleh command bash `unzip`. Untuk menjalankan kedua command bash tersebut, akan digunakan `execv()` sehingga harus melakukan `fork()` terlebih dahulu untuk membuat _child process_. Selanjutnya, akan dilakukan loop sebanyak `numOfProcess` untuk menunggu selesainya _download_, setelah itu akan mengekstrak dan `numOfProcess` akan digunakan lagi untuk menunggu selesainya ekstraksi. Setelah selesai, weapon.zip dan character.zip akan dihapus.
``` c
pid_t download_char, download_weap;
int numOfProcess = 0; 
download_char = fork(); numOfProcess++;

if (download_char == 0){
    printf("Downloading character.zip...\n");
    char *args[] = {"wget", "-P","./", "-o-", "-q", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "character", (char *) 0 };
    execv("/bin/wget", args);
}

else {
    download_weap = fork(); numOfProcess++;

    if (download_weap == 0){
        printf("Downloading weapon.zip...\n");
        char *args[] = {"wget", "-P","./", "-o-", "-q", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapon", (char *) 0 };
        execv("/bin/wget", args);
    }

    else {
        for(int i = 0; i < numOfProcess; i++){
            while ((wait(&status)) > 0);
        }
        numOfProcess = 0;

        pid_t extract_char, extract_weap, create_ws, change_cd;
        extract_char = fork(); numOfProcess++;

        if(extract_char == 0) {
            printf("Extracting character.zip...\n");
            char *args[] = {"unzip", "-q", "character", (char *) 0 };
            execv("/bin/unzip", args);
        }
        else {
            extract_weap = fork(); numOfProcess++;

            if(extract_weap == 0){
                printf("Extracting weapon.zip...\n");
                char *args[] = {"unzip", "-q", "weapon", (char *) 0 };
                execv("/bin/unzip", args);
            }   
            else {
                create_ws = fork(); numOfProcess++;

                if(create_ws == 0){
                    printf("Creating working space gacha_gacha...\n");
                    char *args[] = {"mkdir", "gacha_gacha", (char *) 0 };
                    execv("/bin/mkdir", args);
                }
                else {
                    for(int i = 0; i < numOfProcess; i++){
                        while ((wait(&status)) > 0);
                    }
                    numOfProcess = 0;

                    printf("Successfully unzipped character.zip and weapon.zip.\n");
                    if (remove("character") == 0){
                        if(remove("weapon") == 0)
                            printf("Successfully deleted character.zip and weapon.zip.\n");
                    }

                    chdir("./gacha_gacha");
                    printf("Changing directory...\n");
                }
            }
        }
    }
}
```

## Soal 1.b
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal1/soal1.c)

**Deskripsi:**
Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA

**Pembahasan:**
`total_roll` adalah variabel yang menunjukkan roll ke berapa yang sekarang sedang dilakukan. Maka dari itu, akan dilakukan mengecekan dengan mod 10 untuk melihat apakah perlu dibaut .txt yang baru atau tidak. Apabila hasil dari mod 10 adalah 1, berarti roll sebelumnya adalah kelipatan 10 sehingga harus membuat .txt baru.

Selanjutnya, akan dilakukan pengecekan ganjil/genap terhadap variabel `total_roll`. Apabila ganjil, akan mengambil baris ke-n dari sebuah list yang berisi nama-nama file yang berada di folder directory (yang semuanya ada pada format .json) sesuai dengan angka `num`, yang mana `num` adalah angka random yang sudah digenerate.

Setelah mengambil nama .json yang sesuai, file .json tersebut akan dilakukan _parsing_ untuk mengambil nama dan rarity dari item yang terambil. 
```c
 // bikin .txt
        if(total_roll % 10 == 1){
          // get time
          time_t rawtime;
          struct tm * timeinfo;
      
          time(&rawtime);
          timeinfo = localtime(&rawtime);

          // bikin nama
          memset(str_txt, 0, sizeof str_txt);
          int int_count = ((total_roll / 10) + 1) * 10;
          strcat(str_txt, str_dir);
          sprintf(str_temp, "/%02d:%02d:%02d_gacha_%d.txt", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, int_count);
          strcat(str_txt, str_temp);
          sleep(1);
        }

        // opening the .txt
        FILE *txt_gacha = fopen(str_txt, "a");
        
        if(total_roll%2 == 0){
          // getting weapon
          num = (rand() % (total_char));
          if (num == 0) num++;
          file_weap = popen("find ../weapons", "r");
          for(int i = 0; i < total_weap; i++){
            fgets(path, PATH_MAX, file_weap);
            if(i == num) break;
          }

          // parse json
          long int size = strlen(path);
          path[strlen(path) - 1] = '\0';

          roll = fopen(path, "r");
          fread(buffer, 3072, 1, roll);
          fclose(roll);

          parsed_json = json_tokener_parse(buffer);
          json_object_object_get_ex(parsed_json, "rarity", &rarity);
          json_object_object_get_ex(parsed_json, "name", &name);

          fprintf(txt_gacha, "%d_weapon_%d_%s_%d\n", total_roll, json_object_get_int(rarity), json_object_get_string(name), primogems);
        }
        else {
          // getting character
          num = (rand() % (total_char));
          if (num == 0) num++;
          file_char = popen("find ../characters", "r");
          for(int i = 0; i < total_char; i++){
            fgets(path, PATH_MAX, file_char);
            if(i == num) break;
          }

          // parse json
          long int size = strlen(path);
          path[strlen(path) - 1] = '\0';

          roll = fopen(path, "r");
          fread(buffer, 3072, 1, roll);
          fclose(roll);

          parsed_json = json_tokener_parse(buffer);
          json_object_object_get_ex(parsed_json, "rarity", &rarity);
          json_object_object_get_ex(parsed_json, "name", &name);
          
          fprintf(txt_gacha, "%d_character_%d_%s_%d\n", total_roll, json_object_get_int(rarity), json_object_get_string(name), primogems);
        }

        fclose(txt_gacha);
      }
      fclose(file_char);
      fclose(file_weap);
```

## Soal 1.c
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal1/soal1.c)

**Deskripsi:**
Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}\_gacha\_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total\_gacha\_{jumlah-gacha}, misal total\_gacha\_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

**Pembahasan:**
Untuk membuat directory, akan melakukan pengecekan terlebih dahulu apakah `total_roll` mod 90 adalah 1 atau tidak dengan alasan yang sama dengan pembahasan sebelumnya. Apabila ternyata sama dengan 1, akan dibuat sebuah _child process_ untuk membuat _directory_ baru dengan nama yang sesuai dengan format. 

Sementara itu, untuk pembuatan .txt teknisnya sama seperti penjelasan di soal 1.b dengan penamaan .txt yang sesuai dengan format yang diminta. Pengambilan waktu sekarang ini dilakukan oleh variabel berjenis `time_t`. Setelah dibuat .txt baru, sistem akan menunggu 1 detik supaya antar .txt mempunyai jeda 1 detik.
```c
// soal 1c
        // creating directory
        if(total_roll % 90 == 1){
          pid_t dir_gacha;
          dir_gacha = fork();

          // bikin nama directory
          memset(str_dir, 0, sizeof str_dir);
          int int_count = ((total_roll / 90) + 1) * 90;
          sprintf(str_dir,"./total_gacha_%d", int_count);

          if(dir_gacha == 0){
            // bikin dir
            char *args[] = {"mkdir", str_dir, (char *) 0 };
            execv("/bin/mkdir", args);
          }
          else {
            while ((wait(&status)) > 0);
          }

        }
```
```c
          // bikin nama
          memset(str_txt, 0, sizeof str_txt);
          int int_count = ((total_roll / 10) + 1) * 10;
          strcat(str_txt, str_dir);
          sprintf(str_temp, "/%02d:%02d:%02d_gacha_%d.txt", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, int_count);
          strcat(str_txt, str_temp);
          sleep(1);
```

## Soal 1.d
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal1/soal1.c)

**Deskripsi:**
Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}\_[tipe-item]\_{rarity}\_{name}\_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880

**Pembahasan:**
Sebelum melakukan iterasi untuk menarik item, dibuat sebuat variabel `primogems` yang menyimpan banyaknya primogem dan variabel `roll_taken` yang menyatakan seberapa banyak penarikan akan dilakukan sesuai dengan jumlah primogem yang dimiliki. Setelah itu, pada setiap kali iterasi penarikan item, `primogems` akan dikurangi sebanyak 160. Variabel `primogems` ini nanti akan digunakan bersamaan dengan hasil parsing dan `total_roll` untuk menuliskan hasil/item dari penarikan yang dilakukan oleh gacha.
```c
      int total_roll = 0, roll_taken, primogems = 79000;
      roll_taken = primogems / 160;
```
```c
      // rolling
      for(int i = 0; i < roll_taken; i++){
        total_roll++;
        primogems -= 160;
```
```c

          parsed_json = json_tokener_parse(buffer);
          json_object_object_get_ex(parsed_json, "rarity", &rarity);
          json_object_object_get_ex(parsed_json, "name", &name);
          
          fprintf(txt_gacha, "%d_character_%d_%s_%d\n", total_roll, json_object_get_int(rarity), json_object_get_string(name), primogems);
```

## Soal 1.e
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal1/soal1.c)

**Deskripsi:**
Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

**Pembahasan:**
Untuk melakukan scheduling, saya membuat semacam timer yang menghitung beda detik dari detik sekarang ke detik dari waktu yang diinginkan. Hal itu saya lakukan denga terlebih dahulu mencari selisih waktu hari, jam, menit dan detik kemudian mengubah semua satuannya ke dalam detik dan ditotalkan. Setelah itu, total dari detik yang didapatkan akan digunakan untuk melakukan `sleep()` selama detik tersebut.

Selanjutnya, apabila sudah selesai `sleep()`, penarikan random number gacha dimulai. Setelah selesai, akan melakukan `sleep()` lagi sebanyak 3 jam. Setelah itu, akan melakukan fork untuk membuat _child process_ yang akan membuat zip bernama not\_safe\_for\_wibu dengan passwork "satuduatiga" yang berisi semua isi dari folder gacha\_gacha. Setelah itu, semua directory yang ada akan dihapus dan program daemon akan berhenti berjalan karena `execv()` yang terakhir mengambil alih program daemon untuk menghapus directory.
```c
// soal 1e (scheduling)
      time_t rawtime2;
      struct tm * timeinfo2;
      
      time(&rawtime2);
      timeinfo2 = localtime(&rawtime2);

      int day, hour, minute, second;
      unsigned long long int total_sec;
      int target_day = 30;
      int target_hour = 4;
      int target_minute = 44;

      day = (target_day - timeinfo2->tm_mday);
      hour = (target_hour + 24 - timeinfo2->tm_hour);
      if(hour != 0){
        day--;
      }
      minute = (target_minute + 60 - timeinfo2->tm_min);
      if(minute != 0){
        hour--;
      }
      second = (60- timeinfo2->tm_sec);
      if(minute != 0){
        minute--;
      }

      total_sec = (day * 3600 * 24) + (hour * 3600) + (minute * 60) + second;

      sleep(total_sec);

```
```c
// soal 1e
      // zipping gacha_gacha
      int wait_hour = 3;
      sleep(wait_hour * 3600);
      chdir("..");

      pid_t zipping, rmdir_char, rmdir_weap, rmdir_gacha;
      zipping = fork();
      if(zipping == 0){
        printf("Zipping folder gacha_gacha...\n");
        char *args[] = {"zip", "-q", "-r", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", (char *) 0 };
        
        execv("/bin/zip", args);
      }
      else{
        while ((wait(&status)) > 0);
  
        rmdir_char = fork();
        if(rmdir_char == 0){
          printf("Removing directory characters...\n");
          char *args[] = {"rm", "-rf", "characters", (char *) 0 };
          execv("/bin/rm", args);
        }
        else {
          rmdir_weap = fork();
          if(rmdir_weap == 0){
            printf("Removing directory weapon...\n");
            char *args[] = {"rm", "-rf", "weapons", (char *) 0 };
            execv("/bin/rm", args);
          }
          else {
            rmdir_gacha = fork();
            printf("Removing directory gacha...\n");
            char *args[] = {"rm", "-rf", "gacha_gacha", (char *) 0 };
            execv("/bin/rm", args);
          }
        }
      }
  } 
}
```

#  Soal 2
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal2/soal2.c)

**Deskripsi:**
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat
ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea
yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah
mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review,
tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan
poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan
untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun
untuk menyelesaikan pekerjaannya.

## Soal 2.a
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal2/soal2.c)

**Deskripsi:**
Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang
diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun
teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka
program harus bisa membedakan file dan folder sehingga dapat memproses file yang
seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

**Pembahasan**
Membuat child untuk masing-masing proses yaitu: membuat directory drakor, unzip folder drakor.zip, dan menghapus folder yang tidak penting.

```c
int main(){

    pid_t child;
    int status;

    child = fork();


    if (child < 0) exit(EXIT_FAILURE);
    else if (child == 0){
        char *argv[] = {"mkdir", "/home/roviery/shift2", NULL};
        execv("/bin/mkdir", argv);
    }
    else {
        while((wait(&status)) > 0);

        // 2a
        /*====== Membuat directory drakor =======*/
        child = fork();

        if (child < 0) exit(EXIT_FAILURE);
        else if (child == 0){
            char *argv[] = {"mkdir", "/home/roviery/shift2/drakor", NULL};
            execv("/bin/mkdir", argv);  
        }else while((wait(&status)) > 0);

        /*====== unzip drakor.zip =======*/

        child = fork();

        if (child < 0) exit(EXIT_FAILURE);
        else if (child == 0){
            char *argv[] = {"unzip", "drakor.zip", "-d", "/home/roviery/shift2/drakor", NULL};
            execv("/bin/unzip", argv);  
        }else while((wait(&status)) > 0);

        /*====== delete folder tidak penting =======*/
        
        DIR *dp;
        struct dirent *ep;
        char path[100] = "/home/roviery/shift2/drakor";

        dp = opendir(path);

        if (dp != NULL){
            while ((ep = readdir(dp))){
                char folderPath[100];

                memset(folderPath, 0, strlen(folderPath));
                strcat(folderPath, "/home/roviery/shift2/drakor/");
                strcat(folderPath, ep->d_name);

                if (ep->d_type == 4){
                    child = fork();

                    if (child < 0) exit(EXIT_FAILURE);
                    else if (child == 0){
                        char *argv[] = {"rm", "-rf", folderPath, NULL};
                        execv("/bin/rm", argv); 
                    }else while((wait(&status)) > 0);
                }
            }
        }
```

## Soal 2.b, 2.c, dan 2.d
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal2/soal2.c)

**Deskripsi:**
Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus
membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu
tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan
folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis
drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.

Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder
dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.

Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di
pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama
“start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder
“/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”.


**Pembahasan**
Tiga cabang soal ini dijadikan satu proses yaitu:
* Pembuatan folder genre drakor dengan pembacaan nama file poster yang ada menggunakan directory listing.
* Copy file poster ke dalam directory genre yang sudah dibuat menggunakan directory listing. 
* Pemisahan foto 2 poster dan memasukkannya ke directory masing-masing.

```c
       dp = opendir(path);
        if (dp != NULL){
            while ((ep = readdir(dp))){
                char *folderName;
                char fileName[100];
                char rename[100];
                char renamePath[100];
                int counter = 1;

                strcpy(fileName, ep->d_name);
                folderName = strtok(ep->d_name, ";");

                while(folderName != NULL){
                    if (counter == 1) {
                        strcpy(rename, folderName);
                        strcat(rename, ".png");
                    }
                    if (counter == 3){
                        char folderPath[100];
                        char filePath[100];

                        if (strstr(folderName, "_") != NULL){
                            folderName = strtok(folderName, "_"); // -> tanpa _...

                            memset(folderPath, 0, strlen(folderPath));
                            strcat(folderPath, "/home/roviery/shift2/drakor/");
                            strcat(folderPath, folderName);

                            child = fork();

                            if (child < 0) exit(EXIT_FAILURE);
                            else if (child == 0){
                                char *argv[] = {"mkdir", "-p", folderPath, NULL};
                                execv("/bin/mkdir", argv); 
                            }else while((wait(&status)) > 0);
                        }
                        else if (strstr(folderName, ".png") != NULL){
                            folderName = strtok(folderName, "."); // -> tanpa extension .png

                            memset(folderPath, 0, strlen(folderPath));
                            strcat(folderPath, "/home/roviery/shift2/drakor/");
                            strcat(folderPath, folderName);

                            child = fork();

                            if (child < 0) exit(EXIT_FAILURE);
                            else if (child == 0){
                                char *argv[] = {"mkdir", "-p", folderPath, NULL};
                                execv("/bin/mkdir", argv); 
                            }else while((wait(&status)) > 0);
                        }


                        // copy file ke directory genre

                        memset(filePath, 0, strlen(filePath));
                        strcat(filePath, "/home/roviery/shift2/drakor/");
                        strcat(filePath, fileName);

                        memset(renamePath, 0, strlen(renamePath));
                        strcat(folderPath, "/");
                        strcat(renamePath, folderPath);
                        strcat(folderPath, fileName);
                        strcat(renamePath, rename);

                        child = fork();

                        if (child < 0) exit(EXIT_FAILURE);
                        else if (child == 0){
                            char *argv[] = {"cp", filePath, folderPath, NULL};
                            execv("/bin/cp", argv); 
                        }else {
                            while((wait(&status)) > 0);

                            // rename 

                            child = fork();

                            if (child < 0) exit(EXIT_FAILURE);
                            else if (child == 0){
                                char *argv[] = {"mv", folderPath, renamePath, NULL};
                                execv("/bin/mv", argv);
                            }else while((wait(&status)) > 0);      
                        }

                        break;
                    }
                    folderName = strtok(NULL, ";");
                    counter++;
                }
            }
        }

        // kasus double akhir (love-alarm;2019;romance_who-are-you;2015;school.png) -> who-are-you;2015;school.png
        dp = opendir(path);
        if (dp != NULL){
            while ((ep = readdir(dp))){
                if (strstr(ep->d_name, "_") != NULL){
                    char *folderName;
                    char fileName[100];
                    char rename[100];
                    char renamePath[100];
                    int counter = 1;

                    strcpy(fileName, ep->d_name);
                    folderName = strtok(ep->d_name, "_");

                    while(folderName != NULL){
                        if (counter == 2) break;

                        folderName = strtok(NULL, "_");
                        counter++;
                    }

                    counter = 1;
                    folderName = strtok(folderName, ";");
                    while(folderName != NULL){
                        if (counter == 1){
                            strcpy(rename, folderName);
                            strcat(rename, ".png");
                        }
                        if (counter == 3){
                            char folderPath[100];
                            char filePath[100];
                            if (strstr(folderName, ".png") != NULL){
                                folderName = strtok(folderName, ".");

                                memset(folderPath, 0, strlen(folderPath));
                                strcat(folderPath, "/home/roviery/shift2/drakor/");
                                strcat(folderPath, folderName);

                                child = fork();

                                if (child < 0) exit(EXIT_FAILURE);
                                else if (child == 0){
                                    char *argv[] = {"mkdir", "-p", folderPath, NULL};
                                    execv("/bin/mkdir", argv); 
                                }else while((wait(&status)) > 0);
                            }

                            // copy file ke directory genre

                            memset(filePath, 0, strlen(filePath));
                            strcat(filePath, "/home/roviery/shift2/drakor/");
                            strcat(filePath, fileName);

                            memset(renamePath, 0, strlen(renamePath));
                            strcat(folderPath, "/");
                            strcat(renamePath, folderPath);
                            strcat(folderPath, fileName);
                            strcat(renamePath, rename);

                            child = fork();

                            if (child < 0) exit(EXIT_FAILURE);
                            else if (child == 0){
                                char *argv[] = {"cp", filePath, folderPath, NULL};
                                execv("/bin/cp", argv); 
                            }else {
                                while((wait(&status)) > 0);

                                child = fork();

                                if (child < 0) exit(EXIT_FAILURE);
                                else if (child == 0){
                                    char *argv[] = {"mv", folderPath, renamePath, NULL};
                                    execv("/bin/mv", argv); 
                                }else while((wait(&status)) > 0);    
                            }
                            break;
                        }
                        folderName = strtok(NULL, ";");
                        counter++;
                    }
                }
            }
        }
```

## Soal 2.e
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal2/soal2.c)

**Deskripsi:**
Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama
dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting
list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh
dibawah ini.

**Pembahasan:**
*Pembuatan file Data.txt yang berisi nama foto, tahun rilis, dan genre. Dibaca dengan menggunakan **strtok()** untuk membaca pemisah antara nama file, tahun, dan genre yang berupa titik koma (;).
*Sorting tahun rilis dari isi file Data.txt secara ascending. 

```c
        dp = opendir(path);
        if (dp != NULL){
            while (ep = readdir(dp)){
                if (ep->d_type == 4) continue;
                char *tok;
                char fileName[100];
                char tahun[100];
                char genre[100];
                int counter = 1;

                tok = strtok(ep->d_name, ";");

                while(tok != NULL){
                    if (counter == 1)
                        strcpy(fileName, tok);
                    if (counter == 2)
                        strcpy(tahun, tok);
                    if (counter == 3){
                        if (strstr(tok, "_") != NULL){
                            int c = 1;
                            tok = strtok(tok, "_");
                            strcpy(genre, tok);
                        }
                        else if (strstr(tok, ".png") != NULL){
                            tok = strtok(tok, ".");
                            strcpy(genre, tok);
                        } 
                    }

                    tok = strtok(NULL, ";");
                    counter++;
        
                }

                FILE *outputFile;
                char directoryGenre[100];
                strcpy(directoryGenre, path);
                strcat(directoryGenre, "/");
                strcat(directoryGenre, genre);
                strcat(directoryGenre, "/data.txt");

                outputFile = fopen(directoryGenre, "a");

                fprintf(outputFile, "%s\n", fileName);
                fprintf(outputFile, "%s\n", tahun);

                fclose(outputFile);

            }
        }
    
        // double akhir
        // reply1988;2015;comedy_nightmare-teacher;2016;horror.png
        dp = opendir(path);
        if (dp != NULL){
            while((ep = readdir(dp))){
                if (strstr(ep->d_name, "_") != NULL){
                    
                    if (ep->d_type == 4) continue;
                    char *tok;
                    char fileName[100];
                    char tahun[100];
                    char genre[100];
                    int counter = 1;

                    tok = strtok(ep->d_name, "_");

                    while(tok != NULL){
                        if (counter == 2) break;
                        tok = strtok(NULL, "_");
                        counter++;
                    }

                    counter = 1;
                    tok = strtok(tok, ";");

                    while(tok != NULL){
                        if (counter == 1)
                            strcpy(fileName, tok);
                        if (counter == 2)
                            strcpy(tahun, tok);
                        if (counter == 3){
                            if (strstr(tok, ".png") != NULL){
                                tok = strtok(tok, ".");
                                strcpy(genre, tok);
                            } 
                        }

                        tok = strtok(NULL, ";");
                        counter++;
                    }

                    FILE *outputFile;
                    char directoryGenre[100];
                    strcpy(directoryGenre, path);
                    strcat(directoryGenre, "/");
                    strcat(directoryGenre, genre);
                    strcat(directoryGenre, "/data.txt");
                    outputFile = fopen(directoryGenre, "a");

                    fprintf(outputFile, "%s\n", fileName);
                    fprintf(outputFile, "%s\n", tahun);

                    fclose(outputFile);

                }
            }
        }
    
        // sorting
        dp = opendir(path);
        if (dp != NULL){
            while((ep = readdir(dp))){
                if (ep->d_type == 4){
                    if (strstr(ep->d_name, "..") != NULL) continue;
                    if (strstr(ep->d_name, ".") != NULL) continue;
                    char dataPath[100];
                    char ch;
                    char name[100];
                    char year[100];
                    strcpy(dataPath, path);
                    strcat(dataPath, "/");
                    strcat(dataPath, ep->d_name);
                    strcat(dataPath, "/data.txt");

                    FILE *readFile;
                    readFile = fopen(dataPath, "r");

                    int index = 0;
                    int line = 1; 
                    while(!feof(readFile)){
                        if (line%2 == 1){
                            fscanf(readFile, "%s", drk[index].name);
                            // printf("\nName = %s\n", drk[index].name);
                        }
                        else{
                            fscanf(readFile, "%s", drk[index].year);
                            // printf("\nYear = %s\n", drk[index].year);
                            index++;
                        }
                        line++;
                    }

                    for (int i = 0; i<index-1; i++){
                        for (int j = 0; j<index-i-1 ; j++){
                            int y1 = atoi(drk[j].year);
                            int y2 = atoi(drk[j+1].year);
                            if (y1 > y2){
                                char temp[100];
                                strcpy(temp, drk[j].year);
                                strcpy(drk[j].year, drk[j+1].year);
                                strcpy(drk[j+1].year, temp);

                                strcpy(temp, drk[j].name);
                                strcpy(drk[j].name, drk[j+1].name);
                                strcpy(drk[j+1].name, temp);
                            }

                        }
                    }

                    

                    FILE *writeFile;
                    writeFile = fopen(dataPath, "w");
                    fclose(writeFile);
                    writeFile = fopen(dataPath, "a");

                    fprintf(writeFile, "Kategori: %s\n", ep->d_name);

                    for (int i = 0; i<index; i++){
                        fprintf(writeFile, "\nNama : %s\n", drk[i].name);
                        fprintf(writeFile, "Tahun : %s\n", drk[i].year);

                    }

                    fclose(readFile);
                    fclose(writeFile);
                }
            }
        }
        
        // Delete file
        dp = opendir(path);
        if (dp != NULL){
            while((ep = readdir(dp))){
                if (ep->d_type == 4) continue;

                char filePath[100];

                strcpy(filePath, "/home/roviery/shift2/drakor/");
                strcat(filePath, ep->d_name);

                remove(filePath);
            }
        }
    
    }
```

# Soal 3
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal3/soal3.c)

**Deskripsi:**
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

## Soal 3.a
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal3/soal3.c)

**Deskripsi:**
Membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

**Pembahasan:**
Penyelesaian menggunakan fork() sebanyak dua kali dimana dua child process digunakan untuk membuat directory dengan menggunakan execv().

``` c
int main(){
    pid_t child_id, child_id2;
    int status;

    child_id = fork();
    child_id2 = fork();

    if (child_id < 0)
        exit(EXIT_FAILURE);

    if (child_id2 < 0)
        exit(EXIT_FAILURE);

    if (child_id == 0 && child_id2 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/roviery/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
    else if (child_id != 0 && child_id2 == 0){
        char *argv2[] = {"mkdir", "-p", "/home/roviery/modul2/darat", NULL};
        execv("/bin/mkdir", argv2);
    }

    ...
```
* Karena **fork()** dilakukan 2 kali maka total process terdapat 4.
* **sleep(3)** agar proses pembuatan direktori air dilakukan 3 detik setelah direktori darat

## Soal 3.b
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal3/soal3.c)

**Deskripsi:**
Melakukan extract "animal.zip" di "/home/[USER]/modul2/".

**Pembahasan:**
Extract "animal.zip" dilakukan oleh child pertama dari parent. Unzip dilakukan dengan memanggil fungsi execv().

``` c
    ...
    else if (child_id == 0 && child_id2 != 0){
        while((wait(&status)) > 0);
        char *argv[] = {"unzip", "animal.zip", "-d", "/home/roviery/modul2/", NULL};
        execv("/bin/unzip", argv);
    }
    ...
```
* **while((wait(&status)) > 0)**, agar parent process menunggu semua child process selesai. Dalam kasus ini child processnya merupakan
process yang membuat direktori air. 

## Soal 3.c
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal3/soal3.c)

**Deskripsi:**
Hasil extract dipisah. Untuk hewan darat dimasukkan ke folder darat dan untuk hewan air dimasukkan ke folder air. Rentang pembuatan antara folder darat dan folder air adalah 3 detik (hal ini sudah dilakukan di soal 3a). Untuk hewan yang tidak ada keterangan air atau darat dihapus.

**Pembahasan:**
Penyelesaian menggunakan *Directory Listing* dan exec() setiap membaca file.
``` c
    ...

    else{
        while((wait(&status)) > 0);

        //==============================================================================================//

        DIR *dp;
        struct dirent *ep;
        char path[100] = "/home/roviery/modul2/animal";
        
        dp = opendir(path);

        if (dp != NULL){
            while ((ep = readdir(dp))){
                if (strstr(ep->d_name, "darat") != NULL){
                    char filepath[25];
                    pid_t child_mv;

                    child_mv = fork();

                    memset(filepath,0,strlen(filepath));
                    strcat(filepath, "/home/roviery/modul2/animal/");
                    strcat(filepath, ep->d_name);
                    
                    if (child_mv < 0)
                        exit(EXIT_FAILURE);
                    else if (child_mv == 0){
                        char *argv[] = {"mv", filepath, "/home/roviery/modul2/darat", NULL};
                        execv("/bin/mv", argv);
                    }
                    else{
                        while((wait(&status)) > 0);
                    }
                }
                else if (strstr(ep->d_name, "air") != NULL){
                    char filepath[25];
                    pid_t child_mv;

                    child_mv = fork();

                    memset(filepath,0,strlen(filepath));
                    strcat(filepath, "/home/roviery/modul2/animal/");
                    strcat(filepath, ep->d_name);
                    
                    if (child_mv < 0)
                        exit(EXIT_FAILURE);
                    else if (child_mv == 0){
                        char *argv[] = {"mv", filepath, "/home/roviery/modul2/air", NULL};
                        execv("/bin/mv", argv);
                    }
                    else{
                        while((wait(&status)) > 0);
                    }
                }
                else{
                    char filepath[25];
                    pid_t child_rm;

                    child_rm = fork();

                    memset(filepath,0,strlen(filepath));
                    strcat(filepath, "/home/roviery/modul2/animal/");
                    strcat(filepath, ep->d_name);
                    
                    if (child_rm < 0)
                        exit(EXIT_FAILURE);
                    else if (child_rm == 0){
                        char *argv[] = {"rm", filepath, NULL};
                        execv("/bin/rm", argv);
                    }
                    else{
                        while((wait(&status)) > 0);
                    }
                }
            }
        }
    
    ...

```
* Pemindahan dan Penghapusan file dilakukan oleh Parent Process dimana Parent Process membuat child baru untuk ditugaskan untuk memindahkan atau menghapus file

## Soal 3.d
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal3/soal3.c)

**Deskripsi:**
Menghapus semua file yang ditandai dengan kata "bird" pada nama file.

**Pembahasan:**
Membuat *Directory Listing* baru untuk membaca file pada folder darat. Lalu menghapus filenya ketika menemukan kata "bird" pada nama file.

``` c
    ...

        DIR *dp2;
            struct dirent *ep2;
            char path2[100] = "/home/roviery/modul2/darat";

            dp2 = opendir(path2);

            if (dp2 != NULL){
                while ((ep2 = readdir(dp2))){
                    if (strstr(ep2->d_name, "bird") != NULL){
                        char filepath[25];
                        pid_t child_rm;

                        child_rm = fork();

                        memset(filepath,0,strlen(filepath));
                        strcat(filepath, "/home/roviery/modul2/darat/");
                        strcat(filepath, ep2->d_name);
                        
                        if (child_rm < 0)
                            exit(EXIT_FAILURE);
                        else if (child_rm == 0){
                            char *argv[] = {"rm", filepath, NULL};
                            execv("/bin/rm", argv);
                        }
                        else{
                            while((wait(&status)) > 0);
                        }
                    }
                }
            }

    ...

```


## Soal 3.e
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-2-d10-2022/-/blob/main/soal3/soal3.c)

**Deskripsi:**
Membuat list nama semua hewan di file "list.txt" yang ada di directory “/home/[USER]/modul2/air” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.

**Pembahasan:**
Membuat *Directory Listing* baru untuk membaca folder air dan membuat file list.txt. Setelah sudah dimasukan ke dalam list.txt, list.txt dipindahkan ke folder air.
``` c
    ...

        DIR *dp3;
        struct dirent *ep3;
        char path3[100] = "/home/roviery/modul2/air";

        dp3 = opendir(path3);

        if (dp3 != NULL){
            FILE *outputFile = fopen("list.txt", "w");
            while ((ep3 = readdir(dp3))){

                if (strcmp(ep3->d_name, ".") == 0 || strcmp(ep3->d_name, "..") == 0){
                    continue;
                }

                char filepath[25];
                char formatText[256];
                struct stat fs;
                int r;

                sprintf(formatText, "%d_", (int)getuid());


                memset(filepath,0,strlen(filepath));
                strcat(filepath, "/home/roviery/modul2/air/");
                strcat(filepath, ep3->d_name);

                r = stat(filepath, &fs);
                if (r == -1){
                    fprintf(stderr, "File error\n");
                    exit(1);
                }

                if (fs.st_mode & S_IRUSR)
                    strcat(formatText, "r");
                if (fs.st_mode & S_IWUSR)
                    strcat(formatText, "w");
                if (fs.st_mode & S_IXUSR)
                    strcat(formatText, "x");

                strcat(formatText, "_");
                strcat(formatText, ep3->d_name);
                strcat(formatText, "\n");

                fprintf(outputFile, formatText);

            }

            pid_t child_mv;
            child_mv = fork();
            
            if (child_mv < 0)
                exit(EXIT_FAILURE);
            else if (child_mv == 0){
                char *argv[] = {"mv", "list.txt", "/home/roviery/modul2/air",  NULL};
                execv("/bin/mv", argv);
            }
            else{
                while((wait(&status)) > 0);
            }
        }

    ...
```

