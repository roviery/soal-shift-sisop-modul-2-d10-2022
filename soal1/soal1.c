#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <json-c/json.h>
#include <time.h>

#define PATH_MAX 50

int main() {
  pid_t pid, sid;        // Variabel untuk menyimpan PID

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/home/zahrafayya/Sisop/Modul2/soal1")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {
      int status;

      // soal 1a
      pid_t download_char, download_weap;
      int numOfProcess = 0; 
      download_char = fork(); numOfProcess++;

      if (download_char == 0){
        printf("Downloading character.zip...\n");
        char *args[] = {"wget", "-P","./", "-o-", "-q", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "character", (char *) 0 };
        execv("/bin/wget", args);
      }

      else {
        download_weap = fork(); numOfProcess++;

        if (download_weap == 0){
          printf("Downloading weapon.zip...\n");
          char *args[] = {"wget", "-P","./", "-o-", "-q", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapon", (char *) 0 };
          execv("/bin/wget", args);
        }

        else {
          for(int i = 0; i < numOfProcess; i++){
            while ((wait(&status)) > 0);
          }
          numOfProcess = 0;

          pid_t extract_char, extract_weap, create_ws, change_cd;
          extract_char = fork(); numOfProcess++;

          if(extract_char == 0){
            printf("Extracting character.zip...\n");
            char *args[] = {"unzip", "-q", "character", (char *) 0 };
            execv("/bin/unzip", args);
          }

          else{
            extract_weap = fork(); numOfProcess++;

            if(extract_weap == 0){
              printf("Extracting weapon.zip...\n");
              char *args[] = {"unzip", "-q", "weapon", (char *) 0 };
              execv("/bin/unzip", args);
            }

            else {
              create_ws = fork(); numOfProcess++;

              if(create_ws == 0){
                printf("Creating working space gacha_gacha...\n");
                char *args[] = {"mkdir", "gacha_gacha", (char *) 0 };
                execv("/bin/mkdir", args);
              }
              else {
                for(int i = 0; i < numOfProcess; i++){
                  while ((wait(&status)) > 0);
                }
                numOfProcess = 0;

                printf("Successfully unzipped character.zip and weapon.zip.\n");
                if (remove("character") == 0){
                  if(remove("weapon") == 0)
                    printf("Successfully deleted character.zip and weapon.zip.\n");
                }

                chdir("./gacha_gacha");
                printf("Changing directory...\n");
              }
            }
          }
        }
      }

      // soal 1b, 1c, 1d
      int total_char = 0, total_weap = 0, num;
      char ch, path[PATH_MAX], curr_dir[PATH_MAX];
      FILE *file_char, *file_weap;

      // ngitung jumlah file di folder character
      file_char = popen("ls ../characters", "r");
      while((ch=fgetc(file_char))!=EOF) {
        if(ch=='\n')
          total_char++;
      }

      // ngitung jumlah file di folder weapon
      file_weap = popen("ls ../weapons", "r");
      while((ch=fgetc(file_weap))!=EOF) {
        if(ch=='\n')
          total_weap++;
      }

      int total_roll = 0, roll_taken, primogems = 79000;
      roll_taken = primogems / 160;

      // soal 1e (scheduling)
      time_t rawtime2;
      struct tm * timeinfo2;
      
      time(&rawtime2);
      timeinfo2 = localtime(&rawtime2);

      int day, hour, minute, second;
      unsigned long long int total_sec;
      int target_day = 30;
      int target_hour = 4;
      int target_minute = 44;

      day = (target_day - timeinfo2->tm_mday);
      hour = (target_hour + 24 - timeinfo2->tm_hour);
      if(hour != 0){
        day--;
      }
      minute = (target_minute + 60 - timeinfo2->tm_min);
      if(minute != 0){
        hour--;
      }
      second = (60- timeinfo2->tm_sec);
      if(minute != 0){
        minute--;
      }

      total_sec = (day * 3600 * 24) + (hour * 3600) + (minute * 60) + second;

      sleep(total_sec);

      // printf("%d day %d hour %d minute %d second\n", day, hour, minute, second);

      // rolling
      for(int i = 0; i < roll_taken; i++){
        total_roll++;
        primogems -= 160;

        FILE *roll;
        char str_dir[PATH_MAX], str_txt[130], str_temp[PATH_MAX];
        char *buffer;
        buffer = (char *) malloc(3072);

        struct json_object *parsed_json;
        struct json_object *rarity; 
        struct json_object *name;

        // soal 1c
        // creating directory
        if(total_roll % 90 == 1){
          pid_t dir_gacha;
          dir_gacha = fork();

          // bikin nama directory
          memset(str_dir, 0, sizeof str_dir);
          int int_count = ((total_roll / 90) + 1) * 90;
          sprintf(str_dir,"./total_gacha_%d", int_count);

          if(dir_gacha == 0){
            // bikin dir
            char *args[] = {"mkdir", str_dir, (char *) 0 };
            execv("/bin/mkdir", args);
          }
          else {
            while ((wait(&status)) > 0);
          }

        }

        // bikin .txt
        if(total_roll % 10 == 1){
          // get time
          time_t rawtime;
          struct tm * timeinfo;
      
          time(&rawtime);
          timeinfo = localtime(&rawtime);

          // bikin nama
          memset(str_txt, 0, sizeof str_txt);
          int int_count = ((total_roll / 10) + 1) * 10;
          strcat(str_txt, str_dir);
          sprintf(str_temp, "/%02d:%02d:%02d_gacha_%d.txt", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, int_count);
          strcat(str_txt, str_temp);
          sleep(1);
        }

        // opening the .txt
        FILE *txt_gacha = fopen(str_txt, "a");
        
        if(total_roll%2 == 0){
          // getting weapon
          num = (rand() % (total_char));
          if (num == 0) num++;
          file_weap = popen("find ../weapons", "r");
          for(int i = 0; i < total_weap; i++){
            fgets(path, PATH_MAX, file_weap);
            if(i == num) break;
          }

          // parse json
          long int size = strlen(path);
          path[strlen(path) - 1] = '\0';

          roll = fopen(path, "r");
          fread(buffer, 3072, 1, roll);
          fclose(roll);

          parsed_json = json_tokener_parse(buffer);
          json_object_object_get_ex(parsed_json, "rarity", &rarity);
          json_object_object_get_ex(parsed_json, "name", &name);

          fprintf(txt_gacha, "%d_weapon_%d_%s_%d\n", total_roll, json_object_get_int(rarity), json_object_get_string(name), primogems);
        }
        else {
          // getting character
          num = (rand() % (total_char));
          if (num == 0) num++;
          file_char = popen("find ../characters", "r");
          for(int i = 0; i < total_char; i++){
            fgets(path, PATH_MAX, file_char);
            if(i == num) break;
          }

          // parse json
          long int size = strlen(path);
          path[strlen(path) - 1] = '\0';

          roll = fopen(path, "r");
          fread(buffer, 3072, 1, roll);
          fclose(roll);

          parsed_json = json_tokener_parse(buffer);
          json_object_object_get_ex(parsed_json, "rarity", &rarity);
          json_object_object_get_ex(parsed_json, "name", &name);
          
          fprintf(txt_gacha, "%d_character_%d_%s_%d\n", total_roll, json_object_get_int(rarity), json_object_get_string(name), primogems);
        }

        fclose(txt_gacha);
      }
      fclose(file_char);
      fclose(file_weap);

      // soal 1e
      // zipping gacha_gacha
      int wait_hour = 3;
      sleep(wait_hour * 3600);
      chdir("..");

      pid_t zipping, rmdir_char, rmdir_weap, rmdir_gacha;
      zipping = fork();
      if(zipping == 0){
        printf("Zipping folder gacha_gacha...\n");
        char *args[] = {"zip", "-q", "-r", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", (char *) 0 };
        
        execv("/bin/zip", args);
      }
      else{
        while ((wait(&status)) > 0);
  
        rmdir_char = fork();
        if(rmdir_char == 0){
          printf("Removing directory characters...\n");
          char *args[] = {"rm", "-rf", "characters", (char *) 0 };
          execv("/bin/rm", args);
        }
        else {
          rmdir_weap = fork();
          if(rmdir_weap == 0){
            printf("Removing directory weapon...\n");
            char *args[] = {"rm", "-rf", "weapons", (char *) 0 };
            execv("/bin/rm", args);
          }
          else {
            rmdir_gacha = fork();
            printf("Removing directory gacha...\n");
            char *args[] = {"rm", "-rf", "gacha_gacha", (char *) 0 };
            execv("/bin/rm", args);
          }
        }
      }
  } 
}