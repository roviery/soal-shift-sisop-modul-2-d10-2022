#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>

int main(){
    pid_t child_id, child_id2;
    int status;

    child_id = fork();
    child_id2 = fork();

    if (child_id < 0)
        exit(EXIT_FAILURE);

    if (child_id2 < 0)
        exit(EXIT_FAILURE);

    if (child_id == 0 && child_id2 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/roviery/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
    else if (child_id != 0 && child_id2 == 0){
        char *argv2[] = {"mkdir", "-p", "/home/roviery/modul2/darat", NULL};
        execv("/bin/mkdir", argv2);
    }
    else if (child_id == 0 && child_id2 != 0){
        while((wait(&status)) > 0);
        char *argv[] = {"unzip", "animal.zip", "-d", "/home/roviery/modul2/", NULL};
        execv("/bin/unzip", argv);
    }
    else{
        while((wait(&status)) > 0);

        //==============================================================================================//

        DIR *dp;
        struct dirent *ep;
        char path[100] = "/home/roviery/modul2/animal";
        
        dp = opendir(path);

        if (dp != NULL){
            while ((ep = readdir(dp))){
                if (strstr(ep->d_name, "darat") != NULL){
                    char filepath[25];
                    pid_t child_mv;

                    child_mv = fork();

                    memset(filepath,0,strlen(filepath));
                    strcat(filepath, "/home/roviery/modul2/animal/");
                    strcat(filepath, ep->d_name);
                    
                    if (child_mv < 0)
                        exit(EXIT_FAILURE);
                    else if (child_mv == 0){
                        char *argv[] = {"mv", filepath, "/home/roviery/modul2/darat", NULL};
                        execv("/bin/mv", argv);
                    }
                    else{
                        while((wait(&status)) > 0);
                    }
                }
                else if (strstr(ep->d_name, "air") != NULL){
                    char filepath[25];
                    pid_t child_mv;

                    child_mv = fork();

                    memset(filepath,0,strlen(filepath));
                    strcat(filepath, "/home/roviery/modul2/animal/");
                    strcat(filepath, ep->d_name);
                    
                    if (child_mv < 0)
                        exit(EXIT_FAILURE);
                    else if (child_mv == 0){
                        char *argv[] = {"mv", filepath, "/home/roviery/modul2/air", NULL};
                        execv("/bin/mv", argv);
                    }
                    else{
                        while((wait(&status)) > 0);
                    }
                }
                else{
                    char filepath[25];
                    pid_t child_rm;

                    child_rm = fork();

                    memset(filepath,0,strlen(filepath));
                    strcat(filepath, "/home/roviery/modul2/animal/");
                    strcat(filepath, ep->d_name);
                    
                    if (child_rm < 0)
                        exit(EXIT_FAILURE);
                    else if (child_rm == 0){
                        char *argv[] = {"rm", filepath, NULL};
                        execv("/bin/rm", argv);
                    }
                    else{
                        while((wait(&status)) > 0);
                    }
                }
            }
        }

        //==============================================================================================//

        DIR *dp2;
        struct dirent *ep2;
        char path2[100] = "/home/roviery/modul2/darat";

        dp2 = opendir(path2);

        if (dp2 != NULL){
            while ((ep2 = readdir(dp2))){
                if (strstr(ep2->d_name, "bird") != NULL){
                    char filepath[25];
                    pid_t child_rm;

                    child_rm = fork();

                    memset(filepath,0,strlen(filepath));
                    strcat(filepath, "/home/roviery/modul2/darat/");
                    strcat(filepath, ep2->d_name);
                    
                    if (child_rm < 0)
                        exit(EXIT_FAILURE);
                    else if (child_rm == 0){
                        char *argv[] = {"rm", filepath, NULL};
                        execv("/bin/rm", argv);
                    }
                    else{
                        while((wait(&status)) > 0);
                    }
                }
            }
        }

        //==============================================================================================//

        DIR *dp3;
        struct dirent *ep3;
        char path3[100] = "/home/roviery/modul2/air";

        dp3 = opendir(path3);

        if (dp3 != NULL){
            FILE *outputFile = fopen("list.txt", "w");
            while ((ep3 = readdir(dp3))){

                if (strcmp(ep3->d_name, ".") == 0 || strcmp(ep3->d_name, "..") == 0){
                    continue;
                }

                char filepath[25];
                char formatText[256];
                struct stat fs;
                int r;

                sprintf(formatText, "%d_", (int)getuid());


                memset(filepath,0,strlen(filepath));
                strcat(filepath, "/home/roviery/modul2/air/");
                strcat(filepath, ep3->d_name);

                r = stat(filepath, &fs);
                if (r == -1){
                    fprintf(stderr, "File error\n");
                    exit(1);
                }

                if (fs.st_mode & S_IRUSR)
                    strcat(formatText, "r");
                if (fs.st_mode & S_IWUSR)
                    strcat(formatText, "w");
                if (fs.st_mode & S_IXUSR)
                    strcat(formatText, "x");

                strcat(formatText, "_");
                strcat(formatText, ep3->d_name);
                strcat(formatText, "\n");

                fprintf(outputFile, formatText);

            }

            pid_t child_mv;
            child_mv = fork();
            
            if (child_mv < 0)
                exit(EXIT_FAILURE);
            else if (child_mv == 0){
                char *argv[] = {"mv", "list.txt", "/home/roviery/modul2/air",  NULL};
                execv("/bin/mv", argv);
            }
            else{
                while((wait(&status)) > 0);
            }
        }


    }


    return 0;
}